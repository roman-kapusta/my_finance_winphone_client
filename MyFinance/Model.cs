﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using MyFinance.Data;
using HttpClient = Windows.Web.Http.HttpClient;
using HttpResponseMessage = Windows.Web.Http.HttpResponseMessage;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;

namespace MyFinance
{
	public class Model
	{
		private static readonly string Url = @"https://oxygen-my-finance.herokuapp.com/";

		private static readonly HttpCredentialsHeaderValue Credentials = new HttpCredentialsHeaderValue("Basic",
			Convert.ToBase64String(Encoding.UTF8.GetBytes("roman:kapusta")));

		public async Task<List<TransactionType>> DownloadTransactionTypes()
		{
			HttpResponseMessage response;

			using (var client = new HttpClient())
			{
				client.DefaultRequestHeaders.Authorization = Credentials;

				response = await client.GetAsync(new Uri(Url + "transaction-types/"));
			}

			var jsonFile = await response.Content.ReadAsStringAsync();

			MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonFile));
			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TransactionTypes));
			TransactionTypes transactionTypes = (TransactionTypes)jsonSerializer.ReadObject(ms);

			return transactionTypes.TransTypes.ToList();
		}

		public async Task<bool> CreateTransaction(Transaction transaction)
		{
			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(JsonTransaction));
			var ms = new MemoryStream();
			var jsonTransaction = ConvertTransaction(transaction);

			jsonSerializer.WriteObject(ms, jsonTransaction);

			var json = Encoding.UTF8.GetString(ms.ToArray(), 0, ms.ToArray().Length);
			HttpResponseMessage response;

			using (var client = new HttpClient())
			{
				client.DefaultRequestHeaders.Authorization = Credentials;
				client.DefaultRequestHeaders.Accept.Add(new HttpMediaTypeWithQualityHeaderValue("application/json"));
				HttpStringContent stringContent = new HttpStringContent(json, UnicodeEncoding.Utf8, "application/json");

				response = await client.PostAsync(new Uri(Url + "transaction"), stringContent);
			}

			return response.IsSuccessStatusCode;
		}

		private JsonTransaction ConvertTransaction(Transaction transaction)
		{
			return new JsonTransaction
			{
				amount = transaction.Amount,
				created_on = transaction.CreatedOn.ToString("s"),
				description = transaction.Description,
				scope = transaction.Scope,
				transaction_type = transaction.TransactionType
			};
		}
	}
}
