﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Data;
using MyFinance.Data;

namespace MyFinance.Helpers
{
	public class TransactionTypeToImageUrlConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			string transactionTypeString = ((string)value).Replace(" ", string.Empty);
			TransactionTypeEnum transactionType;

			var result = Enum.TryParse(transactionTypeString, true, out transactionType);

			if (result)
			{
				return string.Format("/ComboImages/{0}.png", TransactionTypeToUrlDictionary[transactionType]);
			}

			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}

		private static readonly Dictionary<TransactionTypeEnum, string> TransactionTypeToUrlDictionary = new Dictionary<TransactionTypeEnum, string>
		{
			{TransactionTypeEnum.Apartment, "apartment"},
			{TransactionTypeEnum.Car, "car"},
			{TransactionTypeEnum.Food, "food"},
			{TransactionTypeEnum.Entertainment, "entertainment"},
			{TransactionTypeEnum.Internet, "internet"},
			{TransactionTypeEnum.Medication, "medication"},
			{TransactionTypeEnum.Clothes, "clothes"},
			{TransactionTypeEnum.HouseholdGoods, "household goods"},
			{TransactionTypeEnum.Other, "other"},
			{TransactionTypeEnum.Sport, "sport"},
			{TransactionTypeEnum.Gifts, "gifts"}
		}; 
	}
}