﻿using System;
using Windows.UI.Xaml.Data;

namespace MyFinance.Helpers
{
	public class EnumToBooleanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			object parameterValue = Enum.Parse(value.GetType(), parameter.ToString());

			return parameterValue.Equals(value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return Enum.Parse(targetType, parameter.ToString());
		}
	}
}