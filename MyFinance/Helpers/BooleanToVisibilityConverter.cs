﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MyFinance.Helpers
{
	public class BooleanToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, System.Type targetType, object parameter, string language)
		{
			return (bool)value ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, System.Type targetType, object parameter, string language)
		{
			throw new System.NotImplementedException();
		}
	}
}