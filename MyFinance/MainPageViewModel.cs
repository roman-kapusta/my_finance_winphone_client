﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MyFinance.Annotations;
using MyFinance.Data;
using MyFinance.Helpers;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace MyFinance
{
	public class MainPageViewModel : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region Private members

		private bool isProgressVisible;
		private bool isCreationVisible;
		private bool isFailVisible;
		private int amount;
		private string description;
		private DateTime transactionDate;
		private ObservableCollection<TransactionType> transactionTypes;
		private ModelCommand addTransactionCommand;
		private ModelCommand cancelCommand;
		private Scope scope;
		private Model model;
		private string message;
		private MainPage mainPage;
		private bool isNetworkIssue;

		#endregion

		#region Props region

		public bool IsNetworkIssue
		{
			get { return isNetworkIssue; }
			set
			{
				if (isNetworkIssue != value)
				{
					isNetworkIssue = value;
					OnPropertyChanged();
				}
			}
		}

		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				if (description != value)
				{
					description = value;
					OnPropertyChanged();
				}
			}
		}

		public string Message
		{
			get
			{
				return message;
			}
			set
			{
				if (message != value)
				{
					message = value;
					OnPropertyChanged();
				}
			}
		}

		public bool IsFailVisible
		{
			get
			{
				return isFailVisible;
			}
			set
			{
				if (isFailVisible != value)
				{
					isFailVisible = value;
					OnPropertyChanged();
				}
			}
		}

		public bool IsProgressVisible
		{
			get
			{
				return isProgressVisible;
			}
			set
			{
				if (isProgressVisible != value)
				{
					isProgressVisible = value;
					OnPropertyChanged();
				}
			}
		}

		public bool IsCreationVisible
		{
			get
			{
				return isCreationVisible;
			}
			set
			{
				if (isCreationVisible != value)
				{
					isCreationVisible = value;
					OnPropertyChanged();
				}
			}
		}

		public int Amount
		{
			get
			{
				return amount;
			}
			set
			{
				if (amount != value)
				{
					amount = value;
					OnPropertyChanged();
					AddTransactionCommand.OnCanExecuteChanged();
				}
			}
		}

		public DateTime TransactionDate
		{
			get
			{
				return transactionDate;
			}
			set
			{
				if (value != transactionDate)
				{
					transactionDate = value;
					OnPropertyChanged();
				}
			}
		}

		public ObservableCollection<TransactionType> TransactionTypes
		{
			get
			{
				return transactionTypes;
			}
			set
			{
				if (transactionTypes != value)
				{
					transactionTypes = value;
					OnPropertyChanged();
				}
			}
		}

		public Scope Scope
		{
			get { return scope; }
			set
			{
				if (scope != value)
				{
					scope = value;
					OnPropertyChanged();
				}
			}
		}

		public ModelCommand AddTransactionCommand
		{
			get
			{
				if (addTransactionCommand == null)
				{
					addTransactionCommand = new ModelCommand(AddTransaction, CanExecute);
				}
				return addTransactionCommand;
			}
		}

		private bool CanExecute(object _)
		{
			return IsCreationVisible && amount > 0 && this.mainPage.TransactionTypesComboBox.SelectedItem != null;
		}

		public ModelCommand CancelCommand
		{
			get
			{
				if (cancelCommand == null)
				{
					cancelCommand = new ModelCommand(Reset, _=> IsCreationVisible);
				}
				return cancelCommand;
			}
		}

		#endregion

		public MainPageViewModel(MainPage mainPage)
		{
			this.mainPage = mainPage;
			model = new Model();
		}

		public async Task DownloadTransactionTypes()
		{
			if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
			{
				IsNetworkIssue = true;
				IsCreationVisible = false;
				IsFailVisible = false;
				return;
			}
			
			IsNetworkIssue = false;

			Scope = Scope.Credit;
			TransactionDate = DateTime.Now;

			if (TransactionTypes == null || TransactionTypes.Count == 0)
			{
				IsCreationVisible = false;
				Message = "Downloading data...";
				IsProgressVisible = true;
				
				var result = await model.DownloadTransactionTypes();

				if (result == null)
				{
					Message = "Somethin went wrong,\nPlease try again later";
					IsFailVisible = true;
				}
				else
				{
					IsProgressVisible = false;
					IsCreationVisible = true;
					AddTransactionCommand.OnCanExecuteChanged();
					CancelCommand.OnCanExecuteChanged();
					TransactionTypes = new ObservableCollection<TransactionType>(result);
				}
			}
			else
			{
				IsCreationVisible = true;
			}
		}

		private async void AddTransaction(object o)
		{
			IsCreationVisible = false;
			IsFailVisible = false;
			Message = "Sending transaction...";
			IsProgressVisible = true;

			var transaction = new Transaction
			{
				Amount = this.Amount,
				CreatedOn = TransactionDate,
				Scope = 2, // (int)this.Scope,
				TransactionType = ((TransactionType) this.mainPage.TransactionTypesComboBox.SelectedItem).Id,
				Description = this.Description
			};

			var result = await model.CreateTransaction(transaction);

			IsProgressVisible = false;
			
			string message = result ? "Transaction created successful." : "Ups, something went wrong.";
			var dialog = new MessageDialog(message)
			{
				Title = "Result", 
				Options = MessageDialogOptions.None
			};


			await dialog.ShowAsync();
			
			Reset(null);
			IsCreationVisible = true;
			AddTransactionCommand.OnCanExecuteChanged();
			CancelCommand.OnCanExecuteChanged();
		}

		private void Reset(object o)
		{
			TransactionDate = DateTime.Now;
			Amount = 0;
			Description = "";
			Scope = Scope.Credit;
			mainPage.TransactionTypesComboBox.SelectedItem = null;
		}

		public void SelectionChanged()
		{
			AddTransactionCommand.OnCanExecuteChanged();
		}
	}
}
