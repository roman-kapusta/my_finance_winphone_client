﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MyFinance
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPage : Page
	{
		public MainPage()
		{
			DataContext = new MainPageViewModel(this);
			InitializeComponent();

			NavigationCacheMode = NavigationCacheMode.Required;
			Application.Current.Resuming += CurrentOnResuming;
		}

		private async void CurrentOnResuming(object sender, object o)
		{
			await ((MainPageViewModel) DataContext).DownloadTransactionTypes();
		}

		/// <summary>
		/// Invoked when this page is about to be displayed in a Frame.
		/// </summary>
		/// <param name="e">Event data that describes how this page was reached.
		/// This parameter is typically used to configure the page.</param>
		protected async override void OnNavigatedTo(NavigationEventArgs e)
		{
			// TODO: Prepare page for display here.

			// TODO: If your application contains multiple pages, ensure that you are
			// handling the hardware Back button by registering for the
			// Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
			// If you are using the NavigationHelper provided by some templates,
			// this event is handled for you.

			try
			{
				await ((MainPageViewModel) DataContext).DownloadTransactionTypes();
			}
			catch (Exception erorr)
			{
				var stack = string.Format("{0}\n{1}", erorr.Message, erorr.StackTrace);
			}
			
		}

		private void TransactionTypesComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			((MainPageViewModel) DataContext).SelectionChanged();
		}
	}
}
