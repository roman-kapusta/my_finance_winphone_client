﻿namespace MyFinance.Data
{
	public enum TransactionTypeEnum
	{
		Apartment,
		Car,
		Food,
		Entertainment,
		Clothes,
		Gifts,
		HouseholdGoods,
		Internet,
		Medication,
		Other,
		Sport
	}
}