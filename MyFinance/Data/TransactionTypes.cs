﻿using System.Runtime.Serialization;

namespace MyFinance.Data
{
	[DataContract]
	public class TransactionTypes
	{
		[DataMember(Name = "transaction_types")]
		public TransactionType[] TransTypes { get; set; }
	}
}