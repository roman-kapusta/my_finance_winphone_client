﻿namespace MyFinance.Data
{
	public enum Scope
	{
		Debit = 1,
		Credit = 2
	}
}