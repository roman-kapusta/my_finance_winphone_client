﻿using System;
using System.Runtime.Serialization;

namespace MyFinance.Data
{
	public class Transaction
	{
		public int Scope { get; set; }
		public int Amount { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Description { get; set; }
		public int TransactionType { get; set; }
	}
}