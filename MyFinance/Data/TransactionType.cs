﻿using System.Runtime.Serialization;

namespace MyFinance.Data
{
	[DataContract]
	public class TransactionType
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }
		[DataMember(Name = "transaction_type")]
		public string TransType { get; set; }
	}
}