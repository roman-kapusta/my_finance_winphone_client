﻿using System.Runtime.Serialization;

namespace MyFinance.Data
{
    [DataContract]
    class JsonTransaction
    {
        [DataMember]
        public int scope { get; set; }
        [DataMember]
        public int amount { get; set; }
        [DataMember]
        public string created_on { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public int transaction_type { get; set; }
    }
}
